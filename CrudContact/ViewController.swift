//
//  ViewController.swift
//  CrudContact
//
//  Created by Afriyandi Setiawan on 09/01/19.
//  Copyright © 2019 Afriyandi Setiawan. All rights reserved.
//

import IGListKit
import SnapKit
import Kingfisher

var crudService = {
    return Connect<CrudServices>()
}()


class ViewController: UIViewController {
    
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self)
    }()
    
    lazy var collectionView: UICollectionView = {
        let collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: ListCollectionViewLayout(stickyHeaders: false, scrollDirection: .vertical, topContentInset: 10, stretchToEdge: true))
        collection.register(CrudContactCollectionViewCell.self, forCellWithReuseIdentifier: "contact")
        collection.backgroundColor = self.view.backgroundColor
        return collection
    }()
    
    lazy var refresher: UIRefreshControl = {
        let refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(fetchData), for: .valueChanged)
        return refresher
    }()
    
    var contactData: [ContactData] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.alwaysBounceVertical = true
        collectionView.addSubview(refresher)
        adapter.collectionView = collectionView
        adapter.dataSource = self
        self.view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (cv) in
            cv.center.equalToSuperview()
            cv.height.equalToSuperview()
            cv.width.equalToSuperview()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fetchData()
    }
    
    @objc
    func fetchData() {
        if !self.refresher.isRefreshing {
            self.refresher.beginRefreshing()
        }
        crudService.request(target: CrudServices.getAllContact(), success: { (result) in
            let data = result["data"].arrayValue
            self.contactData = []
            for item in data {
                self.contactData.append(ContactData(id: item.dictionaryValue["id"]?.stringValue ?? "", data: item))
            }
        }) {
            self.refresher.endRefreshing()
            self.adapter.performUpdates(animated: true, completion: nil)
        }
    }

}

extension ViewController: ListSingleSectionControllerDelegate {
    func didSelect(_ sectionController: ListSingleSectionController, with object: Any) {
        let section = adapter.section(for: sectionController)
        let detail = DetailContactViewController()
        detail.id = contactData[section].id
        self.navigationController?.pushViewController(detail, animated: true)
    }
}

extension ViewController: ListAdapterDataSource {
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return contactData as [ListDiffable]
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        let configureBlock = { (item: Any, cell: UICollectionViewCell) in
            guard let cell = cell as? CrudContactCollectionViewCell, let contact = item as? ContactData, let detail = contact.data.dictionary, let imageURL = detail["photo"]?.stringValue else { return }
            cell.contactImage.kf.setImage(with: URL(string: imageURL == "N/A" ? "https://unsplash.it/\(cell.contactImage.frame.width)/\(cell.contactImage.frame.width)?random" : imageURL))
            cell.firstNameLabel.text = detail["firstName"]?.string
            cell.lastNameLabel.text = detail["lastName"]?.string
        }
        
        let sizeBlock = { (item: Any, context: ListCollectionContext?) -> CGSize in
            guard let context = context else { return CGSize() }
            return CGSize(width: context.containerSize.width, height: 80)
        }
        
        let section = ListSingleSectionController(nibName: CrudContactCollectionViewCell.className, bundle: nil, configureBlock: configureBlock, sizeBlock: sizeBlock)
        
        section.selectionDelegate = self
        
        return section        
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        let label = UILabel(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 50, height: 20)))
        label.center = self.view.center
        label.textAlignment = .center
        label.numberOfLines = 2
        label.text = "Data is empty,\npull to refresh"
        return label
    }
    
}
