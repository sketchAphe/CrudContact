//
//  CrudContactCollectionViewCell.swift
//  CrudContact
//
//  Created by Afriyandi Setiawan on 10/01/19.
//  Copyright © 2019 Afriyandi Setiawan. All rights reserved.
//

import UIKit

class CrudContactCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var contactImage: UIImageView!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async {
            self.contactImage.layer.masksToBounds = true
            self.contactImage.layer.cornerRadius = self.contactImage.frame.width / 2
        }
    }

}
