//
//  CustomNavigation.swift
//  CrudContact
//
//  Created by Afriyandi Setiawan on 10/01/19.
//  Copyright © 2019 Afriyandi Setiawan. All rights reserved.
//

import UIKit

protocol CustomNavigationDelegate: class {
    func deleteContact()
}

class CustomNavigation: UINavigationController, UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    
    lazy var addButton: UIBarButtonItem = {
        let addButton = UIButton(type: .contactAdd)
        addButton.frame = CGRect(origin: CGPoint.zero, size: CGSize(width: 20, height: 20))
        addButton.adjustsImageWhenHighlighted = true
        addButton.addTarget(self, action: #selector(self.addContact), for: .touchUpInside)
        let barItem = UIBarButtonItem(customView: addButton)
        return barItem
    }()
    
    lazy var editButton: UIBarButtonItem = {
        let editButton = UIButton(type: .custom)
        editButton.frame = CGRect(origin: CGPoint.zero, size: CGSize(width: 50, height: 20))
        editButton.adjustsImageWhenHighlighted = true
        editButton.setTitle("Edit", for: .normal)
        editButton.setTitleColor(.gray, for: .normal)
        editButton.addTarget(self, action: #selector(self.addContact), for: .touchUpInside)
        let barItem = UIBarButtonItem(customView: editButton)
        return barItem
    }()
    
    lazy var deleteButton: UIBarButtonItem = {
        let deleteButton = UIButton(type: .custom)
        deleteButton.frame = CGRect(origin: CGPoint.zero, size: CGSize(width: 50, height: 20))
        deleteButton.adjustsImageWhenHighlighted = true
        deleteButton.setTitle("Delete", for: .normal)
        deleteButton.setTitleColor(.red, for: .normal)
        deleteButton.addTarget(self, action: #selector(self.deleteContact), for: .touchUpInside)
        let barItem = UIBarButtonItem(customView: deleteButton)
        return barItem
    }()
    
    weak var cnDelegate: CustomNavigationDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        interactivePopGestureRecognizer?.delegate = self
    }
    
    @objc
    func addContact() {
        let add = AddContactViewController()
        if let detailVC = self.visibleViewController as? DetailContactViewController {
            add.data = detailVC.contactDetail
        }
        self.pushViewController(add, animated: true)
    }
    
    @objc
    func deleteContact() {
        if let _ = self.visibleViewController as? DetailContactViewController {
            cnDelegate?.deleteContact()
        }
    }
    
    //MARK: - UINavigationControllerDelegate
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if viewController == self.viewControllers.first {
            viewController.navigationItem.rightBarButtonItem = addButton
        } else if viewController is DetailContactViewController {
            viewController.navigationItem.rightBarButtonItems = [deleteButton, editButton]
        }
    }
}
