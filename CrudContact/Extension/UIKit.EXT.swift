//
//  UIKit.ext.swift
//  Aerowolf
//
//  Created by Afriyandi Setiawan on 26/05/18.
//  Copyright © 2018 Cizzu inc. All rights reserved.
//


import UIKit

extension UIWindow {
    public var visibleViewController: UIViewController? {
        return self.visibleViewController(from: rootViewController)
    }
    
    func visibleViewController(from viewController: UIViewController?) -> UIViewController? {
        switch viewController {
        case let navigationController as UINavigationController:
            return self.visibleViewController(from: navigationController.visibleViewController)
        case let tabBarController as UITabBarController:
            return self.visibleViewController(from: tabBarController.selectedViewController)
        case let presentingViewController where
            viewController?.presentedViewController != nil:
            return self.visibleViewController(from: presentingViewController?.presentedViewController)
        default:
            return viewController
        }
    }
    
    func set(rootViewController newRootViewController: UIViewController, withTransition transition: CATransition? = nil) {
        
        let previousViewController = rootViewController
        
        if let transition = transition {
            layer.add(transition, forKey: kCATransition)
        }
        
        rootViewController = newRootViewController
        
        if UIView.areAnimationsEnabled {
            UIView.animate(withDuration: CATransaction.animationDuration()) {
                newRootViewController.setNeedsStatusBarAppearanceUpdate()
            }
        } else {
            newRootViewController.setNeedsStatusBarAppearanceUpdate()
        }
        
        if let transitionViewClass = NSClassFromString("UITransitionView") {
            for subview in subviews where subview.isKind(of: transitionViewClass) {
                subview.removeFromSuperview()
            }
        }
        if let previousViewController = previousViewController {
            // Allow the view controller to be deallocated
            previousViewController.dismiss(animated: false) {
                // Remove the root view in case its still showing
                previousViewController.view.removeFromSuperview()
            }
        }
    }
}

extension UIViewController {
    class func fromStoryboard<T: UIViewController>(name: String, identifier: String) -> T? {
        if #available(iOS 1.0, *) {
            return UIStoryboard(name: name, bundle: nil).instantiateViewController(withIdentifier: identifier) as? T
        }
        return nil
    }
}

extension UIAlertController {
    
    func show(animate: Bool = true, completion: (()-> Void)? = nil) {
        presentCustom(animated: animate, completion: completion)
    }
    
    func showToast(animated: Bool, duration: Double, completion: (()-> Void)?) {
        presentCustom(animated: animated) {
            DispatchQueue.main.asyncAfter(deadline: .now() + duration, execute: {
                self.dismiss(animated: animated, completion: completion)
            })
        }
    }
    
    func presentCustom(animated: Bool, completion: (() -> Void)?) {
        if let rootVC = UIApplication.shared.keyWindow?.visibleViewController {
            presentFromController(controller: rootVC, animated: animated, completion: completion)
        }
    }
    
    private func presentFromController(controller: UIViewController, animated: Bool, completion: (() -> Void)?) {
        if let presented = controller.presentedViewController {
            presented.present(self, animated: animated, completion: completion)
            return
        }
        switch controller {
        case let navVC as UINavigationController:
            presentFromController(controller: navVC.visibleViewController!, animated: animated, completion: completion)
            break
        case let tabVC as UITabBarController:
            presentFromController(controller: tabVC.selectedViewController!, animated: animated, completion: completion)
        case _ as UIAlertController:
            return
        default:
            controller.present(self, animated: animated, completion: completion)
        }
    }
}

func showToast(withMessage message: String?, duration: Double? = nil) {
    showToast(withMessage: message, duration: duration, completion: nil)
}

func showToast(withMessage message: String?, duration: Double? = nil, completion: (()-> Void)?) {
    let _duration = duration == nil ? 2 : duration!
    let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
    alert.showToast(animated: true, duration: _duration, completion: completion)
}

extension UIView {
    
    var snapShot: UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0.0)
        self.drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        let snapshot = UIGraphicsGetImageFromCurrentImageContext()
        let jpgRepresent = UIImage(data: snapshot?.pngData() ?? Data())
        UIGraphicsEndImageContext()
        return jpgRepresent
    }
    
    @IBInspectable var backgroundImage: UIImage? {
        get {
            let rect : CGRect = CGRect(x: 0, y: 0, width: 1, height: 1)
            UIGraphicsBeginImageContext(rect.size)
            let context : CGContext = UIGraphicsGetCurrentContext()!
            
            context.setFillColor(UIColor.white.cgColor)
            context.fill(rect)
            
            let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            return image
        }
        set {
            UIGraphicsBeginImageContext(self.frame.size)
            newValue?.draw(in: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height))
            let patternImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            self.backgroundColor = UIColor(patternImage: patternImage)
        }
    }
    
    // Example use: myView.addBorder(toSide: .Left, withColor: UIColor.redColor().CGColor, andThickness: 1.0)
    
    enum ViewSide {
        case Left, Right, Top, Bottom
    }
    
    class func fromNib<T: UIView>() -> T? {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as? T
    }
    
    func addBorder(toSide side: ViewSide, withColor color: CGColor, andThickness thickness: CGFloat) {
        
        let border = CALayer()
        border.backgroundColor = color
        
        switch side {
        case .Left: border.frame = CGRect(x: frame.minX, y: frame.minY, width: thickness, height: frame.height); break
        case .Right: border.frame = CGRect(x: frame.maxX, y: frame.minY, width: thickness, height: frame.height); break
        case .Top: border.frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: thickness); break
        case .Bottom: border.frame = CGRect(x: frame.minX, y: frame.maxY, width: frame.width, height: thickness); break
        }
        
        layer.addSublayer(border)
    }
    
    @discardableResult
    func roundCorners(corners: UIRectCorner, radius: CGFloat) -> CAShapeLayer {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
        return mask
    }
    
    func roundBorder(width: CGFloat, corners: UIRectCorner, radius: CGFloat, color: UIColor) {
        let mask = roundCorners(corners: corners, radius: radius)
        let borderLayer = CAShapeLayer()
        borderLayer.path = mask.path
        borderLayer.strokeColor = color.cgColor
        borderLayer.lineWidth = width
        borderLayer.fillColor = UIColor.clear.cgColor
        self.layer.addSublayer(borderLayer)
    }
    
}

private var flatAssociatedObjectKey: UInt8 = 0
private var maxLengths = [UITextField: Int]()
private var italicPlaceHolderObjectKey: UInt8 = 0

extension UITextField {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var italicPlaceholder:Bool {
        get {
            guard let obj = objc_getAssociatedObject(self, &italicPlaceHolderObjectKey) as? NSNumber else {
                return false
            }
            return obj.boolValue;
        }
        set {
            if (newValue) {
                if self.placeholder != nil {
                    let font = self.font?.italic()
                    let attribute = [
                        NSAttributedString.Key.font : font ?? UIFont.systemFont(ofSize: 12)
                    ]
                    self.attributedPlaceholder = NSAttributedString(string: self.placeholder!, attributes: attribute)
                }
            }
            objc_setAssociatedObject(self, &italicPlaceHolderObjectKey, NSNumber(value: newValue),
                                     objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            
        }
    }
    
    @IBInspectable var maxLength: Int {
        
        get {
            
            guard let length = maxLengths[self]
                else {
                    return Int.max
            }
            return length
        }
        set {
            maxLengths[self] = newValue
            addTarget(
                self,
                action: #selector(limitLength),
                for: UIControl.Event.editingChanged
            )
        }
    }
    
    @objc func limitLength(textField: UITextField) {
        guard let prospectiveText = textField.text,
            prospectiveText.count >= maxLength
            else {
                return
        }
        
        let selection = selectedTextRange
        let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        text = String(prospectiveText[..<maxCharIndex])
        selectedTextRange = selection
    }
}

extension UIButton {
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
}

extension UIColor {
    
    enum GradientStyle {
        case leftToRigth
        case rightToLeft
        case topToBottom
        case bottomToTop
    }
    
    private struct ImageFromColor {
        static var image = UIImage()
    }
    
    var imageColor: UIImage? {
        get {
            return objc_getAssociatedObject(self, &ImageFromColor.image) as? UIImage
        }
        set {
            objc_setAssociatedObject(self, &ImageFromColor.image, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(hex: Int) {
        self.init(
            red: (hex >> 16) & 0xFF,
            green: (hex >> 8) & 0xFF,
            blue: hex & 0xFF
        )
    }
    
    class func gradient(from style: GradientStyle, frame: CGRect, colors: [UIColor]) -> UIColor {
        let gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = frame
        
        gradientLayer.colors = colors.map({ (color) -> CGColor in
            color.cgColor
        })
        
        switch style {
        case .leftToRigth:
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        case .rightToLeft:
            gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.5)
        case .topToBottom:
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
        case .bottomToTop:
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        }
        UIGraphicsBeginImageContext(gradientLayer.bounds.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            if let image = UIGraphicsGetImageFromCurrentImageContext() {
                UIGraphicsEndImageContext()
                return UIColor(patternImage: image)
            }
            UIGraphicsEndImageContext()
        }
        
        return UIColor.black
    }
}

extension BidirectionalCollection where Iterator.Element: Equatable {
    typealias Element = Self.Iterator.Element
    
    func after(_ item: Element, loop: Bool = false) -> Element? {
        if let itemIndex = self.index(of: item) {
            let lastItem: Bool = (index(after:itemIndex) == endIndex)
            if loop && lastItem {
                return self.first
            } else if lastItem {
                return nil
            } else {
                return self[index(after:itemIndex)]
            }
        }
        return nil
    }
    
    func before(_ item: Element, loop: Bool = false) -> Element? {
        if let itemIndex = self.index(of: item) {
            let firstItem: Bool = (itemIndex == startIndex)
            if loop && firstItem {
                return self.last
            } else if firstItem {
                return nil
            } else {
                return self[index(before:itemIndex)]
            }
        }
        return nil
    }
}

func createLocalUrl(forImageNamed name: String) -> URL? {
    
    let fileManager = FileManager.default
    let cacheDirectory = fileManager.urls(for: .cachesDirectory, in: .userDomainMask)[0]
    let url = cacheDirectory.appendingPathComponent("\(name).png")
    let path = url.path
    
    guard fileManager.fileExists(atPath: path) else {
        guard
            let image = UIImage(named: name),
            let data = image.pngData()
            else { return nil }
        
        fileManager.createFile(atPath: path, contents: data, attributes: nil)
        return url
    }
    
    return url
}

extension UIViewController {
    
    private struct SpinnerStruct {
        static var background = UIView(frame: CGRect.zero)
    }
    
    var spinnerBackground: UIView? {
        get {
            return objc_getAssociatedObject(self, &SpinnerStruct.background) as? UIView
        }
        set {
            objc_setAssociatedObject(self, &SpinnerStruct.background, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func showSpinner() {
        spinnerBackground = UIView(frame: UIScreen.main.bounds)
        spinnerBackground?.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        if let _spinner = spinnerBackground {
            let spinner = UIActivityIndicatorView(style: .whiteLarge)
            
            DispatchQueue.main.async {
                self.spinnerBackground?.addSubview(spinner)
                self.view.addSubview(_spinner)
                spinner.startAnimating()
                spinner.center = self.view.center
            }
        }
    }
    
    func hideSpinner() {
        if let _spinnerBG = spinnerBackground {
            _spinnerBG.removeFromSuperview()
        }
    }
}

extension UINavigationController {
    public func pushViewController(
        _ viewController: UIViewController,
        animated: Bool,
        completion: @escaping () -> Void)
    {
        pushViewController(viewController, animated: animated)
        
        guard animated, let coordinator = transitionCoordinator else {
            DispatchQueue.main.async { completion() }
            return
        }
        
        coordinator.animate(alongsideTransition: nil) { _ in completion() }
    }
    
    func popViewController(
        animated: Bool,
        completion: @escaping () -> Void)
    {
        popViewController(animated: animated)
        
        guard animated, let coordinator = transitionCoordinator else {
            DispatchQueue.main.async { completion() }
            return
        }
        
        coordinator.animate(alongsideTransition: nil) { _ in completion() }
    }
}

extension UIDevice {
    var deviceModel: String {
        if let simulatorModelIdentifier = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] { return simulatorModelIdentifier }
        var sysinfo = utsname()
        uname(&sysinfo) // ignore return value
        return String(bytes: Data(bytes: &sysinfo.machine, count: Int(_SYS_NAMELEN)), encoding: .ascii)!.trimmingCharacters(in: .controlCharacters)
    }
}
