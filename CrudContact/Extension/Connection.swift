//
//  Connection.swift
//  Elevenia
//
//  Created by Afriyandi Setiawan on 10/09/18.
//  Copyright © 2018 Afriyandi Setiawan. All rights reserved.
//

import Alamofire
import Moya
import SwiftyJSON

var undefinedError: JSON = ["SYSTEM_MESSAGE": "Undefined"]

class CustomAlamofireManager: Alamofire.SessionManager {
    static let shared: CustomAlamofireManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        configuration.timeoutIntervalForRequest = 30 // seconds
        configuration.timeoutIntervalForResource = 30 // seconds
        configuration.requestCachePolicy = .useProtocolCachePolicy
        return CustomAlamofireManager(configuration: configuration)
    }()
}

struct Connect<T> where T: CustomTarget {
    
    let networkActivityClosure: NetworkActivityPlugin.NetworkActivityClosure = { change, _ in
        switch change {
        case .began:
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
        case .ended:
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
    }
    
    func request(target: T, success successCallback: @escaping (JSON) -> Void, error errorCallback: @escaping (JSON) -> Void = {_ in }, failure failureCallback: @escaping (MoyaError) -> Void = {_ in }, finish isFinish: @escaping () -> Void) {
        #if DEBUG
        let plugins = [NetworkActivityPlugin(networkActivityClosure: networkActivityClosure) as PluginType, NetworkLoggerPlugin(verbose: true)]
        #else
        let plugins = [NetworkActivityPlugin(networkActivityClosure: networkActivityClosure) as PluginType]
        #endif
        
        MoyaProvider<T>(manager: CustomAlamofireManager.shared, plugins: plugins).request(target) { (_response) in
            defer {
                isFinish()
            }
            switch _response {
            case .success(let result):
                guard let success = try? result.filterSuccessfulStatusCodes() else {
                    if let error = try? result.filter(statusCode: 400) {
                        if let json = try? JSON(error.mapJSON()) {
                            errorCallback(json)
                            return
                        }
                    }
                    errorCallback(undefinedError)
                    return
                }
                if let json = try? JSON(success.mapJSON()) {
                    successCallback(json)
                    return
                }
                errorCallback(undefinedError)
            case .failure(let err):
                failureCallback(err)
            }
        }
    }
}

//Task Parameter
struct TaskParameter {
    struct Header {
        static var appVersion = ["X-App-Ver": Bundle.main.AppVersion]
        static var userAgent = ["User-Agent": "\(Bundle.main.AppName)/\(UIDevice.current.deviceModel)-\(UIDevice.current.systemVersion)/\(Bundle.main.AppVersion)"]
    }
}

//Connection Module
protocol Request {
    associatedtype T
    func request(target: T,
                 success successCallback: @escaping (JSON) -> Void,
                 error errorCallback: @escaping (_ statusCode: Error) -> Void,
                 failure failureCallback: @escaping (MoyaError) -> Void,
                 finish isFinish: @escaping () -> Void)
}

protocol CustomTarget: TargetType {
}

extension CustomTarget {
    var baseURL: URL {
        return Bundle.main.baseURL
    }

    var headers: [String : String]? {
        return joinDictionary(from: [ TaskParameter.Header.appVersion, TaskParameter.Header.userAgent]) as? [String: String]
    }
    
    var sampleData: Data {
        return Data()
    }
}

func joinDictionary(from arrayDict: Array<Dictionary<String, Any>>) -> Dictionary<String, Any> {
    var some = Dictionary<String, Any>()
    for dict in arrayDict {
        some += dict
    }
    return some
}

func += <K, V> (left: inout [K : V], right: [K : V]) {
    for (k, v) in right {
        left.updateValue(v, forKey: k)
    }
}
