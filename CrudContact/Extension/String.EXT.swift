//
//  String.EXT.swift
//  SUDistributor
//
//  Created by Afriyandi Setiawan on 20/11/17.
//  Copyright © 2017 Afriyandi Setiawan. All rights reserved.
//

import Foundation
import UIKit

extension String{
    
    var digits: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
    }
    
    var length: Int {
        get {
            return self.count
        }
    }
    
    var html2Attributed: NSAttributedString? {
        guard let data = data(using: String.Encoding.utf8) else {
            return nil
        }
        return try? NSAttributedString(data: data,
                                options: [.documentType: NSAttributedString.DocumentType.html,
                                          .characterEncoding: String.Encoding.utf8.rawValue],
                                documentAttributes: nil)
    }
    
    func printBold(withSize size: CGFloat, color: UIColor = .black) -> NSAttributedString {
        let attrString = NSMutableAttributedString(string: self)
        let attrs = [
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: size),
            NSAttributedString.Key.foregroundColor: color
        ]
        self.enumerateSubstrings(in: self.startIndex..<self.endIndex, options: String.EnumerationOptions.byWords) { (substring, _range, _, _) in
            attrString.addAttributes(attrs, range: NSRange(_range, in: self))
        }
        
        return attrString
    }
    
    func boldTextWithTag(start: String, end: String, from str: String, font: UIFont, size: CGFloat = 12) -> NSAttributedString {
        var attrString = NSMutableAttributedString(string: str)
        
        let attrs = [
            NSAttributedString.Key.font: font.bold() ?? UIFont.boldSystemFont(ofSize: 0),
            NSAttributedString.Key.foregroundColor: UIColor.black
        ]        
        attrString = fixText(attrString, attributeName: NSAttributedString.Key.font as AnyObject, attributeValue: attrs, propsIndicator: start, propsEndIndicator: end)
        return attrString
    }
    
    func fixText(_ inputText:NSMutableAttributedString, attributeName:AnyObject, attributeValue:[NSAttributedString.Key : AnyObject], propsIndicator:String, propsEndIndicator:String)->NSMutableAttributedString{
        var r1 = (inputText.string as NSString).range(of: propsIndicator)
        while r1.location != NSNotFound {
            let r2 = (inputText.string as NSString).range(of: propsEndIndicator)
            if r2.location != NSNotFound  && r2.location > r1.location {
                let r3 = NSMakeRange(r1.location + r1.length, r2.location - r1.location - r1.length)
                inputText.addAttributes(attributeValue, range: r3)
                inputText.replaceCharacters(in: r2, with: "")
                inputText.replaceCharacters(in: r1, with: "")
            } else {
                break
            }
            r1 = (inputText.string as NSString).range(of: propsIndicator)
        }
        return inputText
    }
    
    var localize:String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    
    func localize(_ comment: String = "") -> String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: comment)
    }
    
    func findFromRegex(with format: String) -> [String] {
        if let regex = try? NSRegularExpression(pattern: format, options: .caseInsensitive) {
            let match = regex.matches(in: self, options: [], range: NSRange(location: 0, length: self.length)).map { (match) -> String in
                if let range = Range(match.range, in: self) {
                    return String(self[range])
                }
                return ""
            }
            return match
        }
        return [""]
    }
}

extension UIFont {
    
    var isSupportBold: Bool {
        return fontDescriptor.symbolicTraits.contains(.traitBold)
    }
    
    var isSupportItalic: Bool {
        return fontDescriptor.symbolicTraits.contains(.traitItalic)
    }
    
    var isSupportBoldItalic: Bool {
        return fontDescriptor.symbolicTraits.contains(.traitBold) && fontDescriptor.symbolicTraits.contains(.traitItalic)
    }
    
    func withTraits(traits:UIFontDescriptor.SymbolicTraits...) -> UIFont? {
        guard let descriptor = self.fontDescriptor.withSymbolicTraits(UIFontDescriptor.SymbolicTraits(traits)) else { return nil }
        return UIFont(descriptor: descriptor, size: 0)
    }
    
    func boldItalic() -> UIFont? {
        return withTraits(traits: .traitBold, .traitItalic)
    }

    func bold() -> UIFont? {
        return withTraits(traits: .traitBold)
    }
    
    func italic() -> UIFont? {
        return withTraits(traits: .traitItalic)
    }
}

extension String
{
    func  toDate(dateFormat format: String = "YYYY-MM-DD") -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        if let date = dateFormatter.date(from: self)
        {
            return date
        }
        print("Invalid arguments ! Returning Current Date . ")
        return Date()
    }
}
