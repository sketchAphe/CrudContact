//
//  Foundation.ext.swift
//  Aerowolf
//
//  Created by Afriyandi Setiawan on 26/05/18.
//  Copyright © 2018 Cizzu inc. All rights reserved.
//

import Foundation

func printDebugOnly(_ items: Any..., separator: String = " ", terminator: String = "\n") {
    
    #if DEBUG
    
    var idx = items.startIndex
    let endIdx = items.endIndex
    
    repeat {
        Swift.print(items[idx], separator: separator, terminator: idx == (endIdx - 1) ? terminator : separator)
        idx += 1
    }
        while idx < endIdx
    
    #endif
}

//func callDebugOnly<T>(_ closure: @autoclosure() -> T) -> T {
//    #if DEBUG
//    return closure()
//    #endif
//}

extension Date {
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return tomorrow.month != month
    }
    
    var toString: String {
        let dateFormater = self.changeDate(format: nil)
        return dateFormater.string(from: self)
    }
    
    func toString(format: String) -> String {
        let format = self.changeDate(format: format)
        return format.string(from: self)
    }
    
    func changeDate(format: String?) -> DateFormatter {
        let dateFormat = DateFormatter()
        guard let _format = format else {
            dateFormat.dateStyle = .full
            return dateFormat
        }
        dateFormat.dateFormat = _format
        return dateFormat
    }
    
    func moveDate(withRange range: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: range, to: noon) ?? Date()
    }
}

extension NSObject {
    var className: String {
        return String(describing: self)
    }
    
    class var className: String {
        return String(describing: self)
    }
}

extension Dictionary {
    subscript(i:Int) -> (key:Key,value:Value) {
        get {
            return self[index(startIndex, offsetBy: i)];
        }
    }
}

extension Bool {
    var intValue: Int {
        return self ? 1 : 0
    }
}

extension Data {
    
    private static let mimeTypeSignatures: [UInt8 : (mime: String, ext: String)] = [
        0xFF : ("image/jpeg", ".jpeg"),
        0x89 : ("image/png", ".png"),
        0x47 : ("image/gif", ".gif"),
        0x49 : ("image/tiff", ".tiff"),
        0x4D : ("image/tiff", ".tiff"),
        0x25 : ("application/pdf", ".pdf"),
        0xD0 : ("application/vnd", ".vnd"),
        0x46 : ("text/plain", ".txt"),
        0x00 : ("video/webm", ".webm")
        ]
    
    var mimeTypeByte: UInt8 {
        var c: UInt8 = 0
        copyBytes(to: &c, count: 1)
        return c
    }
    
    var mimeType: String {
        return Data.mimeTypeSignatures[mimeTypeByte]?.mime ?? "application/octet-stream"
    }
    
    var extensionType: String {
        return Data.mimeTypeSignatures[mimeTypeByte]?.ext ?? ""
    }
}

extension Array where Element: Equatable
{
    mutating func move(_ element: Element, to newIndex: Index) {
        if let oldIndex: Int = self.index(of: element) { self.move(from: oldIndex, to: newIndex) }
    }
}

extension Array
{
    mutating func move(from oldIndex: Index, to newIndex: Index) {
        // Don't work for free and use swap when indices are next to each other - this
        // won't rebuild array and will be super efficient.
        if oldIndex == newIndex { return }
        if abs(newIndex - oldIndex) == 1 { return self.swapAt(oldIndex, newIndex) }
        self.insert(self.remove(at: oldIndex), at: newIndex)
    }
}
