//
//  URL.EXT.swift
//  Elevenia
//
//  Created by Afriyandi Setiawan on 10/09/18.
//  Copyright © 2018 Afriyandi Setiawan. All rights reserved.
//

import Foundation

extension Bundle {
    
    var baseURL: URL {
        guard let info = self.infoDictionary,
            let urlString = info["Base URL"] as? String,
            let url = URL(string: urlString) else {
                fatalError("Cannot get base url from info.plist")
        }
        return url
    }
    
    var AppName: String {
        guard let info = self.infoDictionary,
            let appKey = info["CFBundleName"] as? String else {
                fatalError("Cannot get App Key from info.plist")
        }
        
        return appKey
    }
    
    var AppVersion: String {
        guard let info = self.infoDictionary,
            let appKey = info["CFBundleShortVersionString"] as? String else {
                fatalError("Cannot get App Key from info.plist")
        }
        
        return appKey
    }
    
    var AppKey: String {
        guard let info = self.infoDictionary,
            let appKey = info["App Key"] as? String else {
                fatalError("Cannot get App Key from info.plist")
        }
        
        return appKey
    }
}

extension URL {
    static var baseURL: URL {
        return Bundle.main.baseURL
    }
}

extension URL {
    static var documentsURL: URL {
        return try! FileManager
            .default
            .url(for: .documentDirectory,
                 in: .userDomainMask,
                 appropriateFor: nil,
                 create: true)
    }
    
    static var cacheURL: URL {
        return try! FileManager
        .default
        .url(for: .cachesDirectory,
             in: .userDomainMask,
             appropriateFor: nil,
             create: true)
    }
}
