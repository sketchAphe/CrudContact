//
//  AddContactViewController.swift
//  CrudContact
//
//  Created by Afriyandi Setiawan on 10/01/19.
//  Copyright © 2019 Afriyandi Setiawan. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddContactViewController: UIViewController {
    @IBOutlet weak var textFieldFirstName: UITextField!
    @IBOutlet weak var textFieldLastName: UITextField!
    @IBOutlet weak var textFieldPictureURL: UITextField!
    @IBOutlet weak var textFieldAge: UITextField!
    
    var data: JSON?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Add Contact"
        if let _data = data?.dictionaryValue {
            self.title = "Edit Contact"
            textFieldFirstName.text = _data["firstName"]?.string
            textFieldLastName.text = _data["lastName"]?.string
            textFieldAge.text = String(describing: _data["age"]?.int ?? 0)
            textFieldPictureURL.text = _data["photo"]?.string
        }
    }
    
    @IBAction func actionDoSubmit(_ sender: UIButton) {
        showSpinner()
        guard let firstName = textFieldFirstName.text, let lastName = textFieldLastName.text, let pictureURL = textFieldPictureURL.text, let age = textFieldAge.text  else {
            showToast(withMessage: "Field Cannot Empty")
            hideSpinner()
            return
        }
        if let _data = data {
            crudService.request(target: CrudServices.updateContact(id: _data["id"].stringValue, firstName: firstName, lastName: lastName, age: Int(age) ?? 0 , photo: pictureURL), success: { (result) in
                let msg = result.dictionaryValue["message"]?.string
                showToast(withMessage: msg, completion: {
                    self.navigationController?.popViewController(animated: true)
                })
            }, error: { (err) in
                let msg = err.dictionaryValue["message"]?.string
                showToast(withMessage: msg, duration: 3)
            }, failure: { (err) in
                print(err)
            }) {
                self.hideSpinner()
            }
        } else {
            crudService.request(target: CrudServices.saveContact(firstName: firstName, lastName: lastName, age: Int(age) ?? 0, photo: pictureURL), success: { (result) in
                let msg = result.dictionaryValue["message"]?.string
                showToast(withMessage: msg, completion: {
                    self.navigationController?.popViewController(animated: true)
                })
            }, error: { (err) in
                let msg = err.dictionaryValue["message"]?.string
                showToast(withMessage: msg, duration: 3)
            }, failure: { (err) in
                print(err)
            }) {
                self.hideSpinner()
            }
        }
    }
}
