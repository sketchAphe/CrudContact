//
//  DetailContactViewController.swift
//  CrudContact
//
//  Created by Afriyandi Setiawan on 10/01/19.
//  Copyright © 2019 Afriyandi Setiawan. All rights reserved.
//

import UIKit
import SwiftyJSON

class DetailContactViewController: UIViewController {
    
    @IBOutlet weak var imageContact: UIImageView!
    @IBOutlet weak var labelFirstName: UILabel!
    @IBOutlet weak var labelLastName: UILabel!
    @IBOutlet weak var labelAget: UILabel!
    
    var id: String?
    var contactDetail: JSON?

    override func viewDidLoad() {
        super.viewDidLoad()
        if let customNav = self.navigationController as? CustomNavigation {
            customNav.cnDelegate = self
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showSpinner()
        crudService.request(target: CrudServices.getContact(id: id ?? ""), success: { (result) in
            self.contactDetail = result.dictionaryValue["data"]
            DispatchQueue.main.async {
                if let imageURL = self.contactDetail?["photo"].stringValue {
                    self.imageContact.kf.setImage(with: URL(string: imageURL == "N/A" ? "https://unsplash.it/\(self.imageContact.frame.width)/\(self.imageContact.frame.width)?random" : imageURL))
                }
                self.labelFirstName.text = self.contactDetail?["firstName"].string
                self.labelLastName.text = self.contactDetail?["lastName"].string
                self.labelAget.text = String(describing: self.contactDetail?["age"].intValue ?? 0)
            }
        }, error: { (err) in
            let msg = err.dictionaryValue["message"]?.string
            showToast(withMessage: msg, duration: 3) {
                self.navigationController?.popViewController(animated: true)
            }
        }, failure: { (err) in
            
        }) {
            self.hideSpinner()
        }
    }
    
    override func viewWillLayoutSubviews() {
        DispatchQueue.main.async {
            self.imageContact.layer.masksToBounds = true
            self.imageContact.layer.cornerRadius = self.imageContact.frame.width / 2
        }
    }
    
    func doDeleteContact() {
        showSpinner()
        crudService.request(target: CrudServices.deleteContact(id: id ?? ""), success: { (result) in
            let msg = result.dictionaryValue["message"]?.string
            showToast(withMessage: msg, completion: {
                self.navigationController?.popViewController(animated: true)
            })
        }, error: { (err) in
            let msg = err.dictionaryValue["message"]?.string
            showToast(withMessage: msg, duration: 3)
        }, failure: { (err) in
            print(err)
        }) {
            self.hideSpinner()
        }
    }
}

extension DetailContactViewController: CustomNavigationDelegate {
    func deleteContact() {
        let alert = UIAlertController(title: "Delete", message: "Are you sure want to delete?", preferredStyle: UIAlertController.Style.alert)
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        let delete = UIAlertAction(title: "Delete", style: UIAlertAction.Style.destructive, handler: { _ in
            self.doDeleteContact()
        })
        alert.addAction(cancel)
        alert.addAction(delete)
        alert.show()
    }
}
