//
//  CrudContact.services.swift
//  CrudContact
//
//  Created by Afriyandi Setiawan on 09/01/19.
//  Copyright © 2019 Afriyandi Setiawan. All rights reserved.
//

import Moya
import SwiftyJSON

enum CrudServices {
    case getAllContact()
    case saveContact(firstName: String, lastName: String, age: Int, photo: String)
    case deleteContact(id: String)
    case getContact(id: String)
    case updateContact(id: String, firstName: String, lastName: String, age: Int, photo: String)
}

extension CrudServices: CustomTarget {
    var path: String {
        switch self {
        case .getAllContact, .saveContact:
            return "contact"
        case .deleteContact(let id):
            return "contact/\(id)"
        case .getContact(let id):
            return "contact/\(id)"
        case .updateContact(let id, _, _, _, _):
            return "contact/\(id)"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getAllContact, .getContact:
            return .get
        case .saveContact:
            return .post
        case .deleteContact:
            return .delete
        case .updateContact:
            return .put
        }
    }
    
    var task: Task {
        switch self {
        case .getAllContact:
            return .requestParameters(parameters: [:], encoding: URLEncoding.default)
        case .saveContact(let firstName, let lastName, let age, let photo):
            return .requestParameters(parameters: ["firstName": firstName, "lastName": lastName, "age": age, "photo": photo], encoding: JSONEncoding.default)
        case .deleteContact( _):
            return .requestParameters(parameters: [:], encoding: URLEncoding.default)
        case .getContact( _):
            return .requestParameters(parameters: [:], encoding: URLEncoding.default)
        case .updateContact( _, let firstName, let lastName, let age, let photo):
            return .requestParameters(parameters: ["firstName": firstName, "lastName": lastName, "age": age, "photo": photo], encoding: JSONEncoding.default)
        }
    }
    
    
}
