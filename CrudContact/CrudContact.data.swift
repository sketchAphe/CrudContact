//
//  CrudContact.data.swift
//  CrudContact
//
//  Created by Afriyandi Setiawan on 10/01/19.
//  Copyright © 2019 Afriyandi Setiawan. All rights reserved.
//

import IGListKit
import SwiftyJSON

final class ContactData: NSObject {
    let id: String
    let data: JSON
    
    init(id: String, data: JSON) {
        self.id = id
        self.data = data
        super.init()
    }
}

extension ContactData: ListDiffable {
    func diffIdentifier() -> NSObjectProtocol {
        return self.id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        return self === object ? true : self.isEqual(object)
    }
}
